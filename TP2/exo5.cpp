#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	// split
    for(int i = 0; i < origin.size()/2; i++)
    {
        first[i] = origin[i];
    }
    for(int i = origin.size()/2; i < origin.size(); i++)
    {
        second[i-origin.size()/2] = origin[i];
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    if(first.size() > 1)
    {
        splitAndMerge(first);
    }
    if(second.size() > 1)
    {
        splitAndMerge(second);
    }

	// merge
    merge(first, second, origin);

}


void merge(Array& first, Array& second, Array& result)
{

}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}

#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
    int min;
    int indice_min;

    for(int i = 0; i < toSort.size(); i++)
    {
        min = toSort[i];
        indice_min = i;
        for(int j = i; j < toSort.size(); j++)
        {
            if(toSort[j] < min)
            {
               min = toSort[j];
               indice_min = j;
            }
        }
        toSort.swap(i, indice_min);
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}

#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return(nodeIndex*2 + 1);
}

int Heap::rightChild(int nodeIndex)
{
    return(nodeIndex*2 + 2);
}

void Heap::insertHeapNode(int heapSize, int value)
{
        // use (*this)[i] or this->get(i) to get a value at index i
        int i = heapSize;
    (*this)[i] = value;

    int temp;

    while(i>0 && (*this)[i]>(*this)[(i-1)/2]){
        temp = (*this)[i];
        (*this)[i] = (*this)[(i-1)/2];
        (*this)[(i-1)/2] = temp;

        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
        // use (*this)[i] or this->get(i) to get a value at index i
    int i_max = heapSize;
    int i = nodeIndex;

    if(

    if(largest!=i_max){
        temp = (*this)[0];
        (*this)[0] = (*this)[i_max];
        (*this)[i_max] = temp;
        this->heapify(heapSize, largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for(int i=0; i<numbers.size(); i++){
        this->insertHeapNode(numbers.size(), numbers[i]);
    }
}

void Heap::heapSort()
{
    int n = (*this).size();
    for(int i=n-1;i>=0;i--){
        (*this).swap(0,i);
        (*this).heapify(i,0);
    }

}

int main(int argc, char *argv[])
    {
            QApplication a(argc, argv);
        MainWindow::instruction_duration = 50;
        w = new HeapWindow();
            w->show();

            return a.exec();
    }

#include <time.h>
#include <stdio.h>

#include <QApplication>
#include <QDebug>

#include "tp3.h"
#include "HuffmanNode.h"

_TestMainWindow* w = nullptr;
using std::size_t;
using std::string;

void HuffmanHeap::insertHeapNode(int heapSize, unsigned char c, int frequences)
{
    int i = heapSize;
    (*this)[i] = frequences;
    while(i>=0 && this->get(i).frequences > this->get((i-1)/2).frequences)
        this->swap(i, (i-1)/2);
}


void HuffmanNode::insertNode(HuffmanNode* node)
{
    if( this->isLeaf() )
    {
        HuffmanNode* copy = new HuffmanNode(this->character, this->frequences);
        this->character = '\0';
        if (copy->get_value() > this->get_value())
            this->left = copy;
        else
            this->right = copy;
    }
    else
    {
        if (3*node->frequences < this->frequences)
                    this->left->insertNode(node);
                else
                    this->right->insertNode(node);
            }
            /**
             * à chaque insertion on additionne au noeud courant la valeur
             * du noeud à insérer (pour faire en sorte que la valeur du parent soit
             * toujours la somme de ses deux enfants)
             **/

           this->frequences += node->frequences;

        }


void HuffmanNode::processCodes(std::string baseCode)
{
    if (this->isLeaf())
        this->code = baseCode;
    else {
        if (this->left != nullptr)
            this->left->processCodes(baseCode + '0');
        if (this->right != nullptr)
            this->right->processCodes(baseCode + '1');
    }
}

void HuffmanNode::fillCharactersArray(HuffmanNode** nodes_for_chars)
{
    if (!this->left && !this->right)
        nodes_for_chars[this->character] = this;
    else {
        if (this->left)
            this->left->fillCharactersArray(nodes_for_chars);
        if (this->right)
            this->right->fillCharactersArray(nodes_for_chars);
    }
}


void charFrequences(string data, Array& frequences){
    for (int i=0; i<frequences.size(); i++){
        frequences[i]=0;
    }
    for (int j = 0; j<data.size(); j++){
           frequences[j]+=1;
       }
}

void huffmanHeap(Array& frequences, HuffmanHeap& heap, int& heapSize)
{
    heapSize = 0;
    for (int i=0; i<frequences.size(); i++){
        if (frequences[i]!=0){
            heap.insertHeapNode(heapSize, (char)frequences[i], frequences[i]);
        }
        heapSize++;
    }
}


void huffmanDict(HuffmanHeap& heap, int heapSize, HuffmanNode*& dict)
{
    dict = new HuffmanNode(heap[0].character, heap[0].frequences);
    for (int i = 1; i<heapSize; i++){
        dict->insertNode(&heap.get(i));
    }
}


string huffmanEncode(HuffmanNode** characters, string toEncode)
{
    string encoded = "";
    for (int i=0; i<toEncode.size(); i++){
        characters[i]->processCodes(toEncode);
    }
    return encoded;
}

string huffmanDecode(HuffmanNode* dict, string toDecode)
{
    string decoded = "";
    HuffmanNode* copy = dict;
    int i=0;
    while(toDecode[i] != '0'){
        if (copy->isLeaf()){
            decoded += copy->character;
            i++;
            copy = dict;
        } else {
            if (toDecode[i]=='0')
                copy = copy->left;
            else
                copy = copy->right;
        }
    }
    return decoded;
}



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Array::wait_for_operations = false;
    w = new _TestMainWindow();

    string data = "Ouesh, bien ou bien ? Ceci est une chaine de caracteres sans grand interet";

    Array& frequences = w->newArray(256);
    HuffmanHeap heap(256);
    HuffmanNode* dict;
    int i;

    for (i=0; i < (int)frequences.size(); ++i)
        frequences.__set__(i, 0);

    charFrequences(data, frequences);

    for (i=0; i < (int)frequences.size(); ++i)
        if (frequences[i]>0)
            qDebug() << (char)i << ": " << frequences[i];

    int heapSize=0;

    huffmanHeap(frequences, heap, heapSize);
    huffmanDict(heap, heapSize, dict);
    dict->processCodes("");

    HuffmanNode* characters[256];
    memset(characters, 0, 256 * sizeof (HuffmanNode*));
    dict->fillCharactersArray(characters);

    string encoded = huffmanEncode(characters, data);
    string decoded = huffmanDecode(dict, encoded);

    w->addBinaryNode(dict);
    w->updateScene();
    qDebug("Encoded: %s\n", encoded.c_str());
    qDebug("Decoded: %s\n", decoded.c_str());
    w->show();

    return a.exec();
}
